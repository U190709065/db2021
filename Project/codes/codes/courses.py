import mysql.connector

mydb = mysql.connector.connect(
    user='root',
    password='303Gaye303',
    host='127.0.0.1',
    database='smm'
)

filename = "courses_id.txt"

mycursor = mydb.cursor()

with open(filename,encoding="utf8") as file:
    lines = file.readlines()
    i = 0
    for line in lines:
        line = line.split("\t")
        m_id=line[0]
        map_id = line[1]
        difficulty = line[2]
        if difficulty=="easy":
            difficulty=1
        elif difficulty=="normal":
            difficulty=2
        elif difficulty=="expert":
            difficulty=3
        elif difficulty=="superExpert":
            difficulty=4
        else:
            difficulty=None            
        style = line[3]
        maker =line[4]
        if maker == "":
            maker = None
        else:
            maker = int(line[4])
        title = line[5]
        thumbnail = line[6]
        image = line[7]
        creation = line[8]
        
        #print(player_id, image, flag, player_name)
       
        sql = """INSERT INTO courses (m_id,map_id, difficulty_id, style, maker, title, thumbnail, image, creation) VALUES(%s,%s, %s, %s, %s, %s, %s, %s, %s)"""
        val = [m_id,map_id, difficulty, style, maker, title, thumbnail, image, creation]
        
        mycursor.execute(sql, val)

        
        
        i+=1
        if i % 10000 == 0:
            print(i)
    mydb.commit()
    print("done")